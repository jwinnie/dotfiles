# Nord theme
config.source("nord.py")

# Load settings from autoconfig.yml
config.load_autoconfig()
