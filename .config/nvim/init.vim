call plug#begin(stdpath('data') . '/plugged')

Plug 'tpope/vim-sensible'
Plug 'arcticicestudio/nord-vim'
Plug 'jiangmiao/auto-pairs'
Plug 'vim-airline/vim-airline'
Plug 'qpkorr/vim-bufkill'
Plug 'shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

call plug#end()

" Fancy colors
colorscheme nord

" Line numbers
set number
set relativenumber

" Don't clutter my filesystem!
set noswapfile
set nobackup

" Sensible indent
set tabstop=4
set softtabstop=4
set expandtab
set shiftwidth=4
set smarttab
set cindent

" Mouse support
set mouse=a

" Deoplete
let g:deoplete#enable_at_startup = 1
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ deoplete#manual_complete()

" Airline
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

" Buffer management
nnoremap <m-a-tab> :bn<CR>
nnoremap <m-a-q> :BD<CR>

" Save file as root
command W w !pkexec tee % >/dev/null
